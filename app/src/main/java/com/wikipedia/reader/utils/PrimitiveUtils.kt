package com.wikipedia.reader.utils

import androidx.appcompat.app.AppCompatActivity

fun Int.stringFromResource(activity: AppCompatActivity): String {
    return activity.getString(this)
}