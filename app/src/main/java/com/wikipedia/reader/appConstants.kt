package com.wikipedia.reader

const val APP_BASE_URL: String = "https://en.wikipedia.org/"
const val NETWORK_PAGE_SIZE: Int = 10