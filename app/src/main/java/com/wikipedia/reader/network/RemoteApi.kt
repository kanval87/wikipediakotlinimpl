package com.wikipedia.reader.network

import com.haroldadmin.cnradapter.NetworkResponse
import com.wikipedia.reader.data.remote.response.WikiSearchErrorResponse
import com.wikipedia.reader.data.remote.response.WikiSearchKeyResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface RemoteApi : RemoteData {
    @GET("w/api.php") //format=json&action=query&prop=extracts&exintro&explaintext&redirects=1&titles="+keyword
    override suspend fun getWikiFrontPage(
        @Query("format") format: String,
        @Query("action") action: String,
        @Query("prop") prop: String,
        @Query("formatversion") formatversion: Int,
        @Query("titles") titles: String,
    )


    @GET("w/api.php")
    override suspend fun searchWikiPages(
        @Query("gpssearch") gpssearch: String,
        @Query("action") action: String,
        @Query("formatversion") formatversion: Int,
        @Query("generator") generator: String,
        @Query("gpslimit") gpslimit: Int,
        @Query("prop") prop: String,
        @Query("piprop") piprop: String,
        @Query("pithumbsize") pithumbsize: Int,
        @Query("pilimit") pilimit: Int,
        @Query("redirects") redirects: String,
        @Query("wbptterms") wbptterms: String,
        @Query("format") format: String,
        @Query("exsentences") exsentences: Int,
        @Query("explaintext") explaintext: Int,
    ): NetworkResponse<WikiSearchKeyResponse, WikiSearchErrorResponse>
}