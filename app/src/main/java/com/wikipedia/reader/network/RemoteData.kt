package com.wikipedia.reader.network

import com.haroldadmin.cnradapter.NetworkResponse
import com.wikipedia.reader.NETWORK_PAGE_SIZE
import com.wikipedia.reader.data.remote.response.WikiSearchErrorResponse
import com.wikipedia.reader.data.remote.response.WikiSearchKeyResponse

interface RemoteData {
    suspend fun getWikiFrontPage(
        // action=query
        // &format=json&prop=pageimages%7Cpageterms&titles=Albert%20Einstein&formatversion=2
        format: String = "json",
        action: String = "query",
        prop: String = "pageimages%7Cpageterms&titles",
        formatversion: Int = 2,
        titles: String = "apple",
    )

    //https://en.wikipedia.org/w/api.php?
    // action=query
    // &format=json
    // &prop=extracts%7Cpageimages%7Crevisions
    // &titles=Albert%20Einstein
    // &redirects=1&formatversion=2&exsentences=10&exintro=1&explaintext=1&piprop=thumbnail&pithumbsize=300&rvprop=timestamp
    suspend fun searchWikiPages(
        gpssearch: String,
        action: String = "query",
        formatversion: Int = 2,
        generator: String = "prefixsearch",
        gpslimit: Int = NETWORK_PAGE_SIZE,
        prop: String = "pageimages|pageterms|extracts",
        piprop: String = "thumbnail",
        pithumbsize: Int = 500,
        pilimit: Int = 10,
        redirects: String = "",
        wbptterms: String = "description",
        format: String = "json",
        exsentences: Int = 10,
        explaintext : Int =1
    ): NetworkResponse<WikiSearchKeyResponse, WikiSearchErrorResponse>
}