package com.wikipedia.reader.repository.wikiPage

import androidx.paging.PagingSource

interface WikiPageDbData {
    fun insert(toEntity: WikiPageEntity)
    fun hasPostWithSearchKey(searchKey: String): Int
    fun getWikiPagesFromKey(searchKey: String): List<WikiPageEntity>
    fun getWikiPosts(searchKey: String): PagingSource<Int, WikiPageEntity>
}
