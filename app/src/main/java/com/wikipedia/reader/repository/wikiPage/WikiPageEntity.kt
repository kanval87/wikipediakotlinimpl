package com.wikipedia.reader.repository.wikiPage

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class WikiPageEntity(
    @PrimaryKey
    var pageid: Int,
    var title: String,
    var thumbnail: String,
    var descriptions: String
)
