package com.wikipedia.reader.repository

import androidx.room.Database
import androidx.room.RoomDatabase
import com.wikipedia.reader.repository.wikiPage.WikiPageDao
import com.wikipedia.reader.repository.wikiPage.WikiPageEntity

@Database(
    entities = [
        WikiPageEntity::class
    ],
    version = 1
)
abstract class WikipediaDatabase : RoomDatabase() {
    abstract fun WikiPageDao(): WikiPageDao
}
