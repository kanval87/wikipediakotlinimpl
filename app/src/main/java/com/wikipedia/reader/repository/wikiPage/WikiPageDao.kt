package com.wikipedia.reader.repository.wikiPage

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface WikiPageDao : WikiPageDbData {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    override fun insert(toEntity: WikiPageEntity)

    @Query("SELECT COUNT(*) FROM wikipageentity WHERE title LIKE :searchKey")
    override fun hasPostWithSearchKey(searchKey: String): Int

    @Query("SELECT * FROM wikipageentity WHERE title LIKE :searchKey")
    override fun getWikiPagesFromKey(searchKey: String): List<WikiPageEntity>

    @Query("SELECT * FROM wikipageentity WHERE title LIKE :searchKey")
    override fun getWikiPosts(searchKey: String) : PagingSource<Int, WikiPageEntity>
}