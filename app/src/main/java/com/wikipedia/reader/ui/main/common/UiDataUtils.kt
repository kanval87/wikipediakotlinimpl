package com.wikipedia.reader.ui.main.common

sealed class ApiResult<out T : Any> {
    data class Success<out T : Any>(val data: T) : ApiResult<T>()
    data class Error<out T : Any>(val error: T) : ApiResult<T>()
}