package com.wikipedia.reader.ui.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.wikipedia.reader.R
import com.wikipedia.reader.ui.main.search.SearchPage

val TAB_TITLES = arrayOf(
    R.string.tab_text_1,
    R.string.tab_text_2
)

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(lifecycle: Lifecycle, fm: FragmentManager) :
    FragmentStateAdapter(fm, lifecycle) {
    override fun getItemCount(): Int {
        return 1
    }

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            1 -> {
                SearchPage.newInstance()
            }
            else -> SearchPage.newInstance()
        }
    }

}