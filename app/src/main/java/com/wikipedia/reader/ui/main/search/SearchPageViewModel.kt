package com.wikipedia.reader.ui.main.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import com.wikipedia.reader.repository.wikiPage.WikiPageEntity
import com.wikipedia.reader.ui.main.common.ApiResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class SearchPageViewModel @Inject constructor(
    private val searchPageRepository: SearchPageRepository
) : ViewModel() {

    private val _searchResponse = MutableLiveData<ApiResult<*>>()
    val searchResponse: LiveData<ApiResult<*>> = _searchResponse

    fun getWikiFrontPage(searchKey: String) =
        viewModelScope.launch(Dispatchers.IO) {
            val response = searchPageRepository.getWikiFrontPage("%$searchKey%")
            Timber.d("$response")
            viewModelScope.launch(Dispatchers.Main) {
                _searchResponse.value = response
            }
        }

    suspend fun getWikiPosts(searchKey: String): Flow<PagingData<WikiPageEntity>> {

        val response = searchPageRepository.getWikiFrontPage(searchKey)
        Timber.d("$response")
        return when (response) {
            is ApiResult.Error -> {
                flow { response.error }
            }
            is ApiResult.Success -> {
                val postsToShow = searchPageRepository.getWikiPosts(searchKey)
                Timber.d("$postsToShow")
                return postsToShow
            }
        }


    }

}


