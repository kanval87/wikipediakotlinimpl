package com.wikipedia.reader.ui.main.search

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.haroldadmin.cnradapter.NetworkResponse
import com.wikipedia.reader.NETWORK_PAGE_SIZE
import com.wikipedia.reader.network.RemoteData
import com.wikipedia.reader.repository.wikiPage.WikiPageEntity
import com.wikipedia.reader.ui.main.common.ApiResult
import kotlinx.coroutines.flow.Flow
import timber.log.Timber
import javax.inject.Inject

class SearchPageRepository @Inject constructor(
    private val remoteData: RemoteData,
    private val localData: SearchLocalData
) {


    suspend fun getWikiFrontPage(searchKey: String): ApiResult<*> {
        val hasPosts = localData.hasPostWithSearchKey(searchKey)
        Timber.d("hasPosts $hasPosts")
        if (!hasPosts) {
            val response = remoteData.searchWikiPages(searchKey)
            return when (response) {
                is NetworkResponse.Success -> {
                    localData.addInDb(response.body)
                    ApiResult.Success(true)
                }
                is NetworkResponse.ServerError -> {
                    ApiResult.Error(response.error)
                }
                is NetworkResponse.NetworkError -> {
                    ApiResult.Error(response.error)
                }
                is NetworkResponse.UnknownError -> {
                    ApiResult.Error(response.error)
                }
            }

        } else
            return ApiResult.Success(true)


//        val dbPosts = localData.getPostsWithKey(searchKey)
//        Timber.d("$dbPosts")


    }

    fun getWikiPosts(searchKey: String): Flow<PagingData<WikiPageEntity>> {
        val pagerConfig = PagingConfig(
            pageSize = NETWORK_PAGE_SIZE,
            enablePlaceholders = false,
        )
        val pager = Pager(
            config = pagerConfig,
            pagingSourceFactory = { localData.getWikiPosts(searchKey.plus('%')) },
        )

        return pager.flow
    }

}
