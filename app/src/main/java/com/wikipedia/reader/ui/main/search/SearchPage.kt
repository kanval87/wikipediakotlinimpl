package com.wikipedia.reader.ui.main.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.wikipedia.reader.databinding.ItemWikiPageRowBinding
import com.wikipedia.reader.databinding.SearchPageFragmentBinding
import com.wikipedia.reader.repository.wikiPage.WikiPageEntity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SearchPage : Fragment() {

    companion object {
        fun newInstance() = SearchPage()
    }


    private val searchListAdapter: SearchPagerListAdapter = SearchPagerListAdapter()
    private lateinit var localViewBinder: SearchPageFragmentBinding
    private val viewModelSearchPage: SearchPageViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return SearchPageFragmentBinding.inflate(
            inflater,
            container,
            false
        ).apply {
            localViewBinder = this
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }


    private fun initUi() = with(localViewBinder) {
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = searchListAdapter
        }

        searchAutocompleteTextview.doOnTextChanged { text, start, before, count ->
            recyclerView.post {
                recyclerView.visibility = GONE
            }
            if (count != 0) {
                viewModelSearchPage.viewModelScope.launch(Dispatchers.IO) {
                    viewModelSearchPage.getWikiPosts(text.toString()).collect {
                        if (it is PagingData<WikiPageEntity>) {
                            recyclerView.post {
                                recyclerView.visibility = VISIBLE
                            }
                            searchListAdapter.submitData(it)
                        }
                    }
                }
            }
        }

        searchAutocompleteTextview.setText("vahak")
    }
}

class SearchPagerListAdapter :
    PagingDataAdapter<WikiPageEntity, SearchListViewHolder>(WikiPageComparison) {
    override fun onBindViewHolder(holder: SearchListViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchListViewHolder =
        SearchListViewHolder(
            ItemWikiPageRowBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

}

object WikiPageComparison : DiffUtil.ItemCallback<WikiPageEntity>() {
    override fun areItemsTheSame(oldItem: WikiPageEntity, newItem: WikiPageEntity): Boolean {
        return oldItem.pageid == newItem.pageid
    }

    override fun areContentsTheSame(oldItem: WikiPageEntity, newItem: WikiPageEntity): Boolean {
        return oldItem.pageid == newItem.pageid
    }
}

class SearchListViewHolder(private val itemWikiPageRowBinding: ItemWikiPageRowBinding) :
    RecyclerView.ViewHolder(itemWikiPageRowBinding.root) {
    fun bind(wikiPageEntity: WikiPageEntity) = with(itemWikiPageRowBinding) {
        title.text = wikiPageEntity.title
        desc.text = wikiPageEntity.descriptions
        image.load(wikiPageEntity.thumbnail)
    }

}
