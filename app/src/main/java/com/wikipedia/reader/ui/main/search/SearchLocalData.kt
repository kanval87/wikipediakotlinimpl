package com.wikipedia.reader.ui.main.search

import androidx.paging.PagingSource
import com.wikipedia.reader.data.remote.response.WikiSearchKeyResponse
import com.wikipedia.reader.repository.wikiPage.WikiPageDbData
import com.wikipedia.reader.repository.wikiPage.WikiPageEntity
import javax.inject.Inject

class SearchLocalData @Inject constructor(
    private val wikiPageDbData: WikiPageDbData
) {
    fun hasPostWithSearchKey(searchKey: String): Boolean {
        val response: Int = wikiPageDbData.hasPostWithSearchKey(searchKey)
        return response != 0
    }

    fun addInDb(body: WikiSearchKeyResponse) {
        body?.let {
            it.query?.pages?.forEach { wikiPage ->
                wikiPageDbData.insert(wikiPage.toWikiPageEntity())
            }
        }
    }

    fun getPostsWithKey(searchKey: String): List<WikiPageEntity> {
        return wikiPageDbData.getWikiPagesFromKey(searchKey)
    }

    fun getWikiPosts(searchKey: String): PagingSource<Int, WikiPageEntity> {
        return wikiPageDbData.getWikiPosts(searchKey)
    }

}
