package com.wikipedia.reader.data.remote.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class WikiSearchErrorResponse {
    @Json(name = "error")
    var error: WikiApiError? = null

    @Json(name = "servedby")
    var servedby: String? = null
}

@JsonClass(generateAdapter = true)
class WikiApiError {
    @Json(name = "code")
    var code: String? = null

    @Json(name = "info")
    var info: String? = null

    @Json(name = "docref")
    var docref: String? = null
}
