package com.wikipedia.reader.data.remote.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import com.wikipedia.reader.repository.wikiPage.WikiPageEntity

@JsonClass(generateAdapter = true)
class WikiSearchKeyResponse {
    @Json(name = "batchcomplete")
    var batchcomplete = false

    @Json(name = "continue")
    var _continue: WikiContinue? = null

    @Json(name = "query")
    var query: WikiQuery? = null
}

@JsonClass(generateAdapter = true)
class WikiContinue {
    @Json(name = "gpsoffset")
    var gpsoffset = 0

    @Json(name = "continue")
    var wikiContinue: String? = null
}

@JsonClass(generateAdapter = true)
class WikiQuery {
    @Json(name = "pages")
    var pages: List<WikiPage>? = null
}

@JsonClass(generateAdapter = true)
class WikiPage {


    @Json(name = "pageid")
    var pageid = 0

    @Json(name = "ns")
    var ns = 0

    @Json(name = "title")
    var title: String? = null

    @Json(name = "index")
    var index = 0

    @Json(name = "thumbnail")
    var thumbnail: WikiThumbnail? = null

    @Json(name = "terms")
    var terms: WikiTerms? = null

    fun toWikiPageEntity(): WikiPageEntity {
        return WikiPageEntity(
            pageid = pageid,
            title = title.toString(),
            thumbnail = thumbnail?.source.toString(),
            descriptions = terms?.description?.getOrNull(0).toString()
        )
    }


}

@JsonClass(generateAdapter = true)
class WikiThumbnail {
    @Json(name = "source")
    var source: String? = null

    @Json(name = "width")
    var width = 0

    @Json(name = "height")
    var height = 0
}

@JsonClass(generateAdapter = true)
class WikiTerms {

    @Json(name = "description")
    var description: List<String>? = null
}



