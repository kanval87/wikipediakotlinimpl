package com.wikipedia.reader.di

import android.content.Context
import androidx.room.Room
import com.wikipedia.reader.repository.WikipediaDatabase
import com.wikipedia.reader.repository.wikiPage.WikiPageDbData
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DbModuleDi {

    @Singleton
    @Provides
    fun providesWikipediaDb(@ApplicationContext context: Context): WikipediaDatabase {
        val builder = Room.databaseBuilder(
            context,
            WikipediaDatabase::class.java,
            "wiki.db"
        )

        return builder.fallbackToDestructiveMigration().build()
    }

    @Singleton
    @Provides
    fun providesWikiPage(wikipediaDatabase: WikipediaDatabase): WikiPageDbData =
        wikipediaDatabase.WikiPageDao()

}