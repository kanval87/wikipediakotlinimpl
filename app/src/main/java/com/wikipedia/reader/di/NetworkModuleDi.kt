package com.wikipedia.reader.di

import com.haroldadmin.cnradapter.NetworkResponseAdapterFactory
import com.squareup.moshi.Moshi
import com.wikipedia.reader.APP_BASE_URL
import com.wikipedia.reader.network.RemoteApi
import com.wikipedia.reader.network.RemoteData
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkModuleDi {

    @Singleton
    @Provides
    fun providesRetrofit(moshi: Moshi = providesMoshi()): Retrofit {
        val retrofit = Retrofit.Builder()
            .baseUrl(APP_BASE_URL)
            .client(
                OkHttpClient.Builder()
                    .addInterceptor(
                        HttpLoggingInterceptor().apply {
                            level = HttpLoggingInterceptor.Level.BODY
                        }
                    )
                    .build()
            )
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(NetworkResponseAdapterFactory())
            .build()
        return retrofit
    }

    @Singleton
    @Provides
    fun providesMoshi(): Moshi {
        return Moshi.Builder().build()
    }

    @Singleton
    @Provides
    fun providesWebApi(retrofit: Retrofit = providesRetrofit()): RemoteData {
        return retrofit.create(RemoteApi::class.java)
    }

}